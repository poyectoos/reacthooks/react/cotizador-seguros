export const diferenciaAnnos = anno => {
  return new Date().getFullYear() - anno;
}
export const valorDepresiacion = (base, diferencia) => {
  return (base*(diferencia*0.03));
}
export const valorExportacion = marca => {
  let incremento;
  switch (marca) {
    case 'europeo':
      incremento = 1.30;
      break;
    case 'americano':
      incremento = 1.15;
      break;
    case 'Asiatico':
      incremento = 1.05;
      break;

    default:
      break;
  }
  return incremento;
}
export const valorTipoPlan = plan => {
  if (plan === 'basico') {
    return 1.20;
  } else {
    return 1.50;
  }
}